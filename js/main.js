"use strict";

let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv","1", "2", "3", "sea", "user", 23];
const ul = document.getElementsByTagName("ul")[0];

function arrOutput(arr,elmnt = document.body) {
    let new_array = arr.map(function callback(e) {
        const li = document.createElement("li");
        li.textContent = `${e}`;
        elmnt.append(li);
    });
    return new_array;
};

arrOutput(arr,);